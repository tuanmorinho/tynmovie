FROM openjdk:20
COPY target/MasterAdmin-0.0.1-SNAPSHOT.jar MasterAdmin-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/MasterAdmin-0.0.1-SNAPSHOT.jar"]