package tyn.tynmovie;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import tyn.tynmovie.infrastructure.core.InitDB;

@SpringBootApplication
@EnableDiscoveryClient
@RequiredArgsConstructor
public class TynmovieApplication {

	@Autowired
	private final InitDB initDB;

	public static void main(String[] args) {
		SpringApplication.run(TynmovieApplication.class, args);
	}

}
