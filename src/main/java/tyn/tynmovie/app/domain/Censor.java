package tyn.tynmovie.app.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tbl_censor")
public class Censor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String type;
    @Column(columnDefinition="TEXT")
    private String description;
    @Column(columnDefinition="TEXT")
    private String imageIcon;

    public Censor(String type, String description, String imageIcon) {
        this.type = type;
        this.description = description;
        this.imageIcon = imageIcon;
    }

    public static final String _id = "id";
}
