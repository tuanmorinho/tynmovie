package tyn.tynmovie.app.domain;

import jakarta.persistence.*;
import lombok.*;
import tyn.tynmovie.infrastructure.core.BaseEntity;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tbl_genre")
public class Genre extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    public Genre(String name) {
        this.name = name;
    }

    public static final String _id = "id";
}
