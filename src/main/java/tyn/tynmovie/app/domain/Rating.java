package tyn.tynmovie.app.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import tyn.tynmovie.infrastructure.core.BaseEntity;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tbl_rating")
public class Rating extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String description;
    // chỉ dùng api của themoviedb
    private String source;
    private Double voteAverage;

    public Rating(String description, String source, Double voteAverage) {
        this.description = description;
        this.source = source;
        this.voteAverage = voteAverage;
    }
}
