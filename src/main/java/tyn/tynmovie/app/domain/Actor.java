package tyn.tynmovie.app.domain;

import jakarta.persistence.*;
import lombok.*;
import tyn.tynmovie.infrastructure.core.BaseEntity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tbl_actor")
public class Actor extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String firstName;
    private String middleName;
    private String lastName;
    @Temporal(TemporalType.DATE)
    private Date birthDate;

    @ManyToMany
    @JoinTable(
            name = "jtbl_actor_national",
            joinColumns = {@JoinColumn(name = "actor_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "national_id", referencedColumnName = "id")}
    )
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<National> nationals = new HashSet<>();

    public Actor(String firstName, String middleName, String lastName, String birthDate, Set<National> nationals) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.birthDate = !birthDate.isEmpty() ? format.parse(birthDate) : null;
        this.nationals = nationals;
    }
}
