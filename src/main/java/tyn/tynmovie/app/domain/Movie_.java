package tyn.tynmovie.app.domain;

import jakarta.persistence.metamodel.SingularAttribute;
import jakarta.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Movie.class)
public class Movie_ {
    public static volatile SingularAttribute<Movie, String> title;
}
