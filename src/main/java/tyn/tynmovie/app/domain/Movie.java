package tyn.tynmovie.app.domain;

import jakarta.persistence.*;
import jakarta.persistence.metamodel.SingularAttribute;
import lombok.*;
import tyn.tynmovie.app.common.MovieTagEnum;
import tyn.tynmovie.infrastructure.core.BaseEntity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(
    name = "tbl_movie", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"title"}),
    }
)
public class Movie extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
    @Column(columnDefinition="TEXT")
    private String description;
    @Column(columnDefinition="TEXT")
    private String imageThumbnail;
    // list string
    @Column(columnDefinition="TEXT")
    private String otherImages;
    private Integer length;

    @Temporal(TemporalType.DATE)
    private Date releaseDate;
    @Temporal(TemporalType.DATE)
    private Date openingDate;

    @Temporal(TemporalType.DATE)
    private Date shownTillDate;
    @Temporal(TemporalType.DATE)
    private Date shownStartDate;

    @Enumerated(EnumType.STRING)
    private MovieTagEnum tag;

    @ManyToMany
    @JoinTable(
            name = "jtbl_movie_actor",
            joinColumns = {@JoinColumn(name = "movie_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "actor_id", referencedColumnName = "id")}
    )
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Actor> actors = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "jtbl_movie_director",
            joinColumns = {@JoinColumn(name = "movie_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "director_id", referencedColumnName = "id")}
    )
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Director> directors = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "jtbl_movie_language",
            joinColumns = {@JoinColumn(name = "movie_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "language_id", referencedColumnName = "id")}
    )
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Language> languages = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "jtbl_movie_national",
            joinColumns = {@JoinColumn(name = "movie_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "national_id", referencedColumnName = "id")}
    )
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<National> nationals = new HashSet<>();

    @OneToOne
    @JoinTable(
            name = "jtbl_movie_rating",
            joinColumns = {@JoinColumn(name = "movie_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "rating_id", referencedColumnName = "id")}
    )
    private Rating rating;

    @ManyToMany
    @JoinTable(
            name = "jtbl_movie_genre",
            joinColumns = {@JoinColumn(name = "movie_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "genre_id", referencedColumnName = "id")}
    )
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<Genre> genres = new HashSet<>();

    @ManyToOne
    @JoinTable(
            name = "jtbl_movie_censor",
            joinColumns = {@JoinColumn(name = "movie_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "censor_id", referencedColumnName = "id")}
    )
    private Censor censor;

    public Movie(String title,
                 String description,
                 String imageThumbnail,
                 String otherImages,
                 Integer length,
                 String releaseDate,
                 String openingDate,
                 String shownStartDate,
                 String shownTillDate,
                 MovieTagEnum tag,
                 Set<Actor> actors,
                 Set<Director> directors,
                 Set<Language> languages,
                 Set<National> nationals,
                 Rating rating,
                 Set<Genre> genres,
                 Censor censor) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        this.title = title;
        this.description = description;
        this.imageThumbnail = imageThumbnail;
        this.otherImages = otherImages;
        this.length = length;
        this.releaseDate = !releaseDate.isEmpty() ? format.parse(releaseDate) : null;
        this.openingDate = !openingDate.isEmpty() ? format.parse(openingDate) : null;
        this.shownTillDate = !shownTillDate.isEmpty() ? format.parse(shownTillDate) : null;
        this.shownStartDate = !shownStartDate.isEmpty() ? format.parse(shownStartDate) : null;
        this.tag = tag;
        this.actors = actors;
        this.directors = directors;
        this.languages = languages;
        this.nationals = nationals;
        this.rating = rating;
        this.genres = genres;
        this.censor = censor;
    }

    public static final String _title = "title";
    public static final String _shownStartDate = "shownStartDate";
    public static final String _shownTillDate = "shownTillDate";
    public static final String _tag = "tag";
    public static final String _censor = "censor";
    public static final String _genre = "genre";
}
