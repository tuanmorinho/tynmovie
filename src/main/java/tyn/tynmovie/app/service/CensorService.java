package tyn.tynmovie.app.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import tyn.tynmovie.app.domain.Censor;
import tyn.tynmovie.app.dto.CensorPageRequest;
import tyn.tynmovie.app.dto.ListCensorResponse;
import tyn.tynmovie.app.repository.CensorRepository;
import tyn.tynmovie.infrastructure.config.exception.RestException;
import tyn.tynmovie.infrastructure.core.PageResponse;
import tyn.tynmovie.infrastructure.utility.PageUtil;

@Service
@RequiredArgsConstructor
public class CensorService {

    @Autowired
    public final CensorRepository censorRepository;

    public ResponseEntity<PageResponse<Censor, ListCensorResponse>> findCensorAdmin(CensorPageRequest request) {
        try {
            final Page<Censor> censors = censorRepository.findAll(PageUtil.getPageRequest(request));
            final PageResponse<Censor, ListCensorResponse> response = new PageResponse<>(censors, Censor.class, ListCensorResponse.class);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            throw new RestException("error.internal_server_error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
