package tyn.tynmovie.app.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import tyn.tynmovie.app.domain.Genre;
import tyn.tynmovie.app.dto.GenrePageRequest;
import tyn.tynmovie.app.dto.ListGenreResponse;
import tyn.tynmovie.app.repository.GenreRepository;
import tyn.tynmovie.infrastructure.config.exception.RestException;
import tyn.tynmovie.infrastructure.core.PageResponse;
import tyn.tynmovie.infrastructure.utility.PageUtil;

@Service
@RequiredArgsConstructor
public class GenreService {

    @Autowired
    public final GenreRepository genreRepository;

    public ResponseEntity<PageResponse<Genre, ListGenreResponse>> findGenreAdmin(GenrePageRequest request) {
        try {
            final Page<Genre> genres = genreRepository.findAll(PageUtil.getPageRequest(request));
            final PageResponse<Genre, ListGenreResponse> response = new PageResponse<>(genres, Genre.class, ListGenreResponse.class);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            throw new RestException("error.internal_server_error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
