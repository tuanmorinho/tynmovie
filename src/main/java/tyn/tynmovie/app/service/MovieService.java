package tyn.tynmovie.app.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import tyn.tynmovie.app.common.ShowTimeConstant;
import tyn.tynmovie.app.domain.Movie;
import tyn.tynmovie.app.dto.ListMovieAdminResponse;
import tyn.tynmovie.app.dto.ListMovieResponse;
import tyn.tynmovie.app.dto.MoviePageRequest;
import tyn.tynmovie.app.dto.MovieResponse;
import tyn.tynmovie.app.repository.MovieRepository;
import tyn.tynmovie.app.specification.CensorSpecification;
import tyn.tynmovie.app.specification.MovieSpecification;
import tyn.tynmovie.infrastructure.config.exception.RestException;
import tyn.tynmovie.infrastructure.core.PageResponse;
import tyn.tynmovie.infrastructure.utility.PageUtil;
import tyn.tynmovie.infrastructure.utility.SpecificationUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MovieService {

    @Autowired
    public final MovieRepository movieRepository;
    @Autowired
    private final MovieSpecification movieSpecification;
    @Autowired
    private final CensorSpecification censorSpecification;

    public ResponseEntity<PageResponse<Movie, ListMovieResponse>> findMovie(MoviePageRequest request) {
        try {
            if (request.getShowFilter() != null && request.getShowFilter().equals(ShowTimeConstant.COMING_SOON)) {
                Page<Movie> movies = movieRepository.findAllCommingSoon(new Date(), PageUtil.getPageRequest(request));
                return mappingMovieResponse(movies, ListMovieResponse.class);
            }
            if (request.getShowFilter() != null && request.getShowFilter().equals(ShowTimeConstant.NOW_SHOWING)) {
                Page<Movie> movies = movieRepository.findAllNowShowing(new Date(), PageUtil.getPageRequest(request));
                return mappingMovieResponse(movies, ListMovieResponse.class);
            }
            Page<Movie> movies = movieRepository.findAll(PageUtil.getPageRequest(request));
            return mappingMovieResponse(movies, ListMovieResponse.class);
        } catch (Exception e) {
            throw new RestException("error.internal_server_error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<PageResponse<Movie, ListMovieAdminResponse>> findMovieAdmin(MoviePageRequest request) {
        try {
            final List<Specification<Movie>> conditions = new ArrayList<>();

            if (request.getShowFilter() != null && request.getShowFilter().equals(ShowTimeConstant.COMING_SOON)) {
                conditions.add(movieSpecification.greaterThan(Movie._shownStartDate, new Date()));
            }

            if (request.getShowFilter() != null && request.getShowFilter().equals(ShowTimeConstant.NOW_SHOWING)) {
                conditions.add(movieSpecification.lessThanOrEqualTo(Movie._shownStartDate, new Date()));
                conditions.add(movieSpecification.greaterThanOrEqualTo(Movie._shownTillDate, new Date()));
            }

            if (request.getSearch() != null) {
                conditions.add(movieSpecification.likeStr(Movie._title, request.getSearch()));
            }

            if (request.getTag() != null) {
                conditions.add(movieSpecification.eq(Movie._tag, request.getTag()));
            }

            if (request.getCensor() != null) {
                conditions.add(movieSpecification.eqCensorId(request.getCensor()));
            }

            if (request.getGenre() != null) {
                conditions.add(movieSpecification.eqGenreId(request.getGenre()));
            }

            Page<Movie> movies = movieRepository.findAll(SpecificationUtil.andSpec(conditions), PageUtil.getPageRequest(request));
            return mappingMovieResponse(movies, ListMovieAdminResponse.class);
        } catch (Exception e) {
            throw new RestException("error.internal_server_error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<MovieResponse> getMovie(Integer id) {
        final Movie movie = movieRepository.findById(id)
                .orElseThrow(() -> new RestException("error.id_not_found", HttpStatus.BAD_REQUEST, id));
        return new ResponseEntity<>(new MovieResponse(movie), HttpStatus.OK);
    }

    private <S> ResponseEntity<PageResponse<Movie, S>> mappingMovieResponse(Page<Movie> movies, Class<S> classS) {
        final PageResponse<Movie, S> response = new PageResponse<>(movies, Movie.class, classS);
        if (response.getContent().isEmpty()) {
            return new ResponseEntity<>(response, HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
