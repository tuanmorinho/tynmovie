package tyn.tynmovie.app.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tyn.tynmovie.app.domain.Genre;
import tyn.tynmovie.app.dto.GenrePageRequest;
import tyn.tynmovie.app.dto.ListGenreResponse;
import tyn.tynmovie.app.service.GenreService;
import tyn.tynmovie.infrastructure.core.PageResponse;

@RestController
@RequestMapping("/adminGenre")
@RequiredArgsConstructor
public class GenreAdController {
    @Autowired
    private final GenreService genreService;

    @PostMapping("findGenre")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'EDITOR')")
    public ResponseEntity<PageResponse<Genre, ListGenreResponse>> findGenreAdmin(@RequestBody GenrePageRequest request) {
        return genreService.findGenreAdmin(request);
    }
}
