package tyn.tynmovie.app.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tyn.tynmovie.app.domain.Movie;
import tyn.tynmovie.app.dto.ListMovieResponse;
import tyn.tynmovie.app.dto.MoviePageRequest;
import tyn.tynmovie.app.dto.MovieResponse;
import tyn.tynmovie.app.service.MovieService;
import tyn.tynmovie.infrastructure.core.PageResponse;

@RestController
@RequestMapping("/movie")
@RequiredArgsConstructor
public class MovieController {

    @Autowired
    private final MovieService movieService;

    @PostMapping
    public ResponseEntity<PageResponse<Movie, ListMovieResponse>> findMovie(@RequestHeader("User-Agent") String platform, @RequestBody MoviePageRequest request) {
        System.out.println(platform);
        return movieService.findMovie(request);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MovieResponse> getMovie(@PathVariable Integer id) {
        return movieService.getMovie(id);
    }
}
