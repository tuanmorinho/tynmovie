package tyn.tynmovie.app.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tyn.tynmovie.app.domain.Movie;
import tyn.tynmovie.app.dto.ListMovieAdminResponse;
import tyn.tynmovie.app.dto.MoviePageRequest;
import tyn.tynmovie.app.service.MovieService;
import tyn.tynmovie.infrastructure.core.PageResponse;

@RestController
@RequestMapping("/adminMovie")
@RequiredArgsConstructor
public class MovieAdController {

    @Autowired
    private final MovieService movieService;

    @PostMapping("findMovie")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'EDITOR')")
    public ResponseEntity<PageResponse<Movie, ListMovieAdminResponse>> findMovieAdmin(@RequestBody MoviePageRequest request) {
        return movieService.findMovieAdmin(request);
    }

}
