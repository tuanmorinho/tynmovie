package tyn.tynmovie.app.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tyn.tynmovie.app.domain.Censor;
import tyn.tynmovie.app.dto.CensorPageRequest;
import tyn.tynmovie.app.dto.ListCensorResponse;
import tyn.tynmovie.app.service.CensorService;
import tyn.tynmovie.infrastructure.core.PageResponse;

@RestController
@RequestMapping("/adminCensor")
@RequiredArgsConstructor
public class CensorAdController {
    @Autowired
    private final CensorService censorService;

    @PostMapping("findCensor")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'EDITOR')")
    public ResponseEntity<PageResponse<Censor, ListCensorResponse>> findCensorAdmin(@RequestBody CensorPageRequest request) {
        return censorService.findCensorAdmin(request);
    }
}
