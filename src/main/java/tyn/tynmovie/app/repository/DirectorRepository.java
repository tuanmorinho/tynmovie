package tyn.tynmovie.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tyn.tynmovie.app.domain.Director;

import java.util.Optional;

@Repository
public interface DirectorRepository extends JpaRepository<Director, Integer> {
    Optional<Director> findByFirstNameAndMiddleNameAndLastName(String firstName, String middleName, String lastName);
}
