package tyn.tynmovie.app.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tyn.tynmovie.app.domain.Movie;

import java.util.Date;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer>, JpaSpecificationExecutor<Movie> {

    // COMMING_SOON
    @Query("SELECT m FROM Movie m WHERE m.shownStartDate >= :now")
    Page<Movie> findAllCommingSoon(Date now, Pageable pageable);

    // NOW_SHOWING
    @Query("SELECT m FROM Movie m WHERE m.shownStartDate <= :now AND m.shownTillDate >= :now")
    Page<Movie> findAllNowShowing(Date now, Pageable pageable);
}
