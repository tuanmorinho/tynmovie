package tyn.tynmovie.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tyn.tynmovie.app.domain.Actor;

import java.util.Optional;

@Repository
public interface ActorRepository extends JpaRepository<Actor, Integer> {
    Optional<Actor> findByFirstNameAndMiddleNameAndLastName(String firstName, String middleName, String lastName);

}
