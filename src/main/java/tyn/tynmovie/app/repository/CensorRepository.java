package tyn.tynmovie.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tyn.tynmovie.app.domain.Censor;

import java.util.Optional;

@Repository
public interface CensorRepository extends JpaRepository<Censor, Integer> {
    Optional<Censor> findByType(String type);
}
