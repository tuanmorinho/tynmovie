package tyn.tynmovie.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tyn.tynmovie.app.domain.National;

import java.util.List;
import java.util.Optional;

@Repository
public interface NationalRepository extends JpaRepository<National, Integer> {

    Optional<National> findByNational(String national);

    List<National> findByNationalIn(List<String> national);
}
