package tyn.tynmovie.app.specification;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import tyn.tynmovie.app.domain.Censor;
import tyn.tynmovie.app.domain.Genre;
import tyn.tynmovie.app.domain.Movie;
import tyn.tynmovie.infrastructure.core.AbstractSpecification;

import java.util.Objects;

@Component
public class MovieSpecification extends AbstractSpecification<Movie> {
    public <A> Specification<Movie> eqCensorId(A value) {
        Objects.requireNonNull(value);
        return ((root, query, cb) -> cb.equal(root.get(Movie._censor).get(Censor._id), value));
    }

    public <A> Specification<Movie> eqGenreId(A value) {
        Objects.requireNonNull(value);
        return (root, query, cb) -> cb.equal(root.get(Movie._genre).get(Genre._id), value);
    }
}
