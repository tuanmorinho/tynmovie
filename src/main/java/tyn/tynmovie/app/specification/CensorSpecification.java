package tyn.tynmovie.app.specification;

import org.springframework.stereotype.Component;
import tyn.tynmovie.app.domain.Censor;
import tyn.tynmovie.infrastructure.core.AbstractSpecification;

@Component
public class CensorSpecification extends AbstractSpecification<Censor> {
}
