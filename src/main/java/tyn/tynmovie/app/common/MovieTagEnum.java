package tyn.tynmovie.app.common;

import lombok.Getter;

@Getter
public enum MovieTagEnum {
    HOT("0", "Hot", "#D25E52", "80px"),
    OUTSTAND("1", "Nổi bật", "#D9B43A", "100px"),
    OUTDATE("2", "Dừng chiếu", "transparent", "0px");

    private String code;
    private String value;
    private String color;
    private String width;

    MovieTagEnum(String code, String value, String color, String width) {
        this.code = code;
        this.value = value;
        this.color = color;
        this.width = width;
    }
}
