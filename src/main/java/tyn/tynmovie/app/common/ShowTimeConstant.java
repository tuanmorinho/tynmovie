package tyn.tynmovie.app.common;

public class ShowTimeConstant {

    public static final Integer COMING_SOON = 1;
    public static final Integer NOW_SHOWING = 2;

}
