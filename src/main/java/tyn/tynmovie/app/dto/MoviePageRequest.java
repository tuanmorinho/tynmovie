package tyn.tynmovie.app.dto;

import lombok.Data;
import tyn.tynmovie.app.common.MovieTagEnum;
import tyn.tynmovie.infrastructure.core.FindAllBaseRequest;

@Data
public class MoviePageRequest extends FindAllBaseRequest {
    private Integer showFilter;
    private String search;
    private MovieTagEnum tag;
    private Integer censor;
    private Integer genre;
}
