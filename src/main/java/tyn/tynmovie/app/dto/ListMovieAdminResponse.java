package tyn.tynmovie.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import tyn.tynmovie.app.common.MovieTagEnum;
import tyn.tynmovie.app.domain.*;

import java.util.Date;
import java.util.Set;

@Data
@AllArgsConstructor
public class ListMovieAdminResponse {

    private Integer id;
    private String title;
    private String description;
    private String imageThumbnail;
    private String otherImages;
    private Integer length;
    private Date releaseDate;
    private Date openingDate;
    private Date shownTillDate;
    private Date shownStartDate;
    private MovieTagEnum tag;
    private Set<Actor> actors;
    private Set<Director> directors;
    private Set<Language> languages;
    private Set<National> nationals;
    private Rating rating;
    private Set<Genre> genres;
    private Censor censor;

    public ListMovieAdminResponse(Movie response) {
        this.id = response.getId();
        this.title = response.getTitle();
        this.description = response.getDescription();
        this.imageThumbnail = response.getImageThumbnail();
        this.otherImages = response.getOtherImages();
        this.length = response.getLength();
        this.releaseDate = response.getReleaseDate();
        this.openingDate = response.getOpeningDate();
        this.shownTillDate = response.getShownTillDate();
        this.shownStartDate = response.getShownStartDate();
        this.tag = response.getTag();
        this.actors = response.getActors();
        this.directors = response.getDirectors();
        this.languages = response.getLanguages();
        this.nationals = response.getNationals();
        this.rating = response.getRating();
        this.genres = response.getGenres();
        this.censor = response.getCensor();
    }
}
