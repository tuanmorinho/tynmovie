package tyn.tynmovie.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import tyn.tynmovie.app.domain.Censor;

@Data
@AllArgsConstructor
public class ListCensorResponse {
    private Integer id;
    private String type;
    private String description;
    private String imageIcon;

    public ListCensorResponse(Censor response) {
        this.id = response.getId();
        this.type = response.getType();
        this.description = response.getDescription();
        this.imageIcon = response.getImageIcon();
    }
}
