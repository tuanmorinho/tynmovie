package tyn.tynmovie.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import tyn.tynmovie.app.common.MovieTagEnum;
import tyn.tynmovie.app.domain.*;

@Data
@AllArgsConstructor
public class ListMovieResponse {
    private Integer id;
    private String title;
    private String imageThumbnail;
    private Integer length;
    private Censor censor;
    private MovieTagEnum tag;
    private String tagColor;
    private String tagWidth;

    public ListMovieResponse(Movie response) {
        this.id = response.getId();
        this.title = response.getTitle();
        this.imageThumbnail = response.getImageThumbnail();
        this.length = response.getLength();
        this.censor = response.getCensor();
        this.tag = response.getTag();
        this.tagColor = response.getTag().getColor();
        this.tagWidth = response.getTag().getWidth();
    }
}
