package tyn.tynmovie.app.dto;

import lombok.Data;
import tyn.tynmovie.infrastructure.core.FindAllBaseRequest;

@Data
public class GenrePageRequest extends FindAllBaseRequest {
}
