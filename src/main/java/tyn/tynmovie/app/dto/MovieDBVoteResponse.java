package tyn.tynmovie.app.dto;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;

@Getter
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, property="@class")
public class MovieDBVoteResponse {
}
