package tyn.tynmovie.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import tyn.tynmovie.app.common.MovieTagEnum;
import tyn.tynmovie.app.domain.*;

import java.util.Date;
import java.util.Set;

@Data
@AllArgsConstructor
public class MovieResponse {

    private Integer id;
    private String title;
    private String description;
    private Date releaseDate;
    private Date openingDate;
    private String imageThumbnail;
    private String otherImages;
    private Integer length;
    private MovieTagEnum tag;
    private String tagColor;
    private Set<Actor> actors;
    private Set<Director> directors;
    private Set<Language> languages;
    private Set<National> nationals;
    private Set<Genre> genres;
    private Rating rating;
    private Censor censor;

    public MovieResponse(Movie response) {
        this.id = response.getId();
        this.title = response.getTitle();
        this.description = response.getDescription();
        this.releaseDate = response.getReleaseDate();
        this.openingDate = response.getOpeningDate();
        this.imageThumbnail = response.getImageThumbnail();
        this.otherImages = response.getOtherImages();
        this.length = response.getLength();
        this.actors = response.getActors();
        this.directors = response.getDirectors();
        this.languages = response.getLanguages();
        this.nationals = response.getNationals();
        this.genres = response.getGenres();
        this.rating = response.getRating();
        this.censor = response.getCensor();
        this.tag = response.getTag();
        this.tagColor = response.getTag().getColor();
    }
}
