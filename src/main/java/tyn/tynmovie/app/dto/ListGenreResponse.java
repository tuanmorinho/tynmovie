package tyn.tynmovie.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import tyn.tynmovie.app.domain.Genre;

@Data
@AllArgsConstructor
public class ListGenreResponse {
    private Integer id;
    private String name;

    public ListGenreResponse(Genre response) {
        this.id = response.getId();
        this.name = response.getName();
    }
}
