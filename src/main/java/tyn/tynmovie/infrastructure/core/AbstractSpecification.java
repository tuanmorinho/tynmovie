package tyn.tynmovie.infrastructure.core;

import jakarta.persistence.metamodel.SingularAttribute;
import org.springframework.data.jpa.domain.Specification;

import java.util.Objects;
import java.util.function.Consumer;

public abstract class AbstractSpecification<T> {

    // like
    public <A> Specification<T> likeStr(SingularAttribute<T, String> field, String value) {
        Objects.requireNonNull(value);
        return ((root, query, cb) -> cb.like(cb.lower(root.get(field).as(String.class)), "%" + value.toLowerCase() + "%"));
    }

    public <A> Specification<T> likeStr(String field, String value) {
        Objects.requireNonNull(value);
        return ((root, query, cb) -> cb.like(cb.lower(root.get(field).as(String.class)), "%" + value.toLowerCase() + "%"));
    }


    // equal
    public <A> Specification<T> eq(SingularAttribute<T, A> field, A value) {
        Objects.requireNonNull(value);
        return ((root, query, cb) -> cb.equal(root.get(field), value));
    }

    public <A> Specification<T> eq(String field, A value) {
        Objects.requireNonNull(value);
        return ((root, query, cb) -> cb.equal(root.get(field), value));
    }

    public <A> Specification<T> eq(SingularAttribute<T, A> first, SingularAttribute<T, A> second) {
        return ((root, query, cb) -> cb.equal(root.get(first), root.get(second)));
    }

    public <A> Specification<T> eq(String first, String second) {
        return ((root, query, cb) -> cb.equal(root.get(first), root.get(second)));
    }


    // less than
    public <A extends Comparable<? super A>> Specification<T> lessThan(
            SingularAttribute<T, A> field, A value) {
        Objects.requireNonNull(value);
        return ((root, query, cb) -> cb.lessThan(root.get(field), value));
    }

    public <A extends Comparable<? super A>> Specification<T> lessThan(
            String field, A value) {
        Objects.requireNonNull(value);
        return ((root, query, cb) -> cb.lessThan(root.get(field), value));
    }

    public <A extends Comparable<? super A>> Specification<T> lessThan(
            SingularAttribute<T, A> first, SingularAttribute<T, A> second) {
        return ((root, query, cb) -> cb.lessThan(root.get(first), root.get(second)));
    }

    public <A extends Comparable<? super A>> Specification<T> lessThan(
            String first, String second) {
        return ((root, query, cb) -> cb.lessThan(root.get(first), root.get(second)));
    }

    public <A extends Comparable<? super A>> Specification<T> lessThanOrEqualTo(
            SingularAttribute<T, A> field, A value) {
        Objects.requireNonNull(value);
        return ((root, query, cb) -> cb.lessThanOrEqualTo(root.get(field), value));
    }

    public <A extends Comparable<? super A>> Specification<T> lessThanOrEqualTo(
            String field, A value) {
        Objects.requireNonNull(value);
        return ((root, query, cb) -> cb.lessThanOrEqualTo(root.get(field), value));
    }

    public <A extends Comparable<? super A>> Specification<T> lessThanOrEqualTo(
            SingularAttribute<T, A> first, SingularAttribute<T, A> second) {
        return ((root, query, cb) -> cb.lessThanOrEqualTo(root.get(first), root.get(second)));
    }

    public <A extends Comparable<? super A>> Specification<T> lessThanOrEqualTo(
            String first, String second) {
        return ((root, query, cb) -> cb.lessThanOrEqualTo(root.get(first), root.get(second)));
    }


    // greater than
    public <A extends Comparable<? super A>> Specification<T> greaterThan(
            SingularAttribute<T, A> field, A value) {
        return ((root, query, cb) -> cb.greaterThan(root.get(field), value));
    }

    public <A extends Comparable<? super A>> Specification<T> greaterThan(
            String field, A value) {
        return ((root, query, cb) -> cb.greaterThan(root.get(field), value));
    }

    public <A extends Comparable<? super A>> Specification<T> greaterThan(
            SingularAttribute<T, A> first, SingularAttribute<T, A> second) {
        return ((root, query, cb) -> cb.greaterThan(root.get(first), root.get(second)));
    }

    public <A extends Comparable<? super A>> Specification<T> greaterThan(
            String first, String second) {
        return ((root, query, cb) -> cb.greaterThan(root.get(first), root.get(second)));
    }

    public <A extends Comparable<? super A>> Specification<T> greaterThanOrEqualTo(
            SingularAttribute<T, A> field, A value) {
        return ((root, query, cb) -> cb.greaterThanOrEqualTo(root.get(field), value));
    }

    public <A extends Comparable<? super A>> Specification<T> greaterThanOrEqualTo(
            String field, A value) {
        return ((root, query, cb) -> cb.greaterThanOrEqualTo(root.get(field), value));
    }

    public <A extends Comparable<? super A>> Specification<T> greaterThanOrEqualTo(
            SingularAttribute<T, A> first, SingularAttribute<T, A> second) {
        return ((root, query, cb) -> cb.greaterThanOrEqualTo(root.get(first), root.get(second)));
    }

    public <A extends Comparable<? super A>> Specification<T> greaterThanOrEqualTo(
            String first, String second) {
        return ((root, query, cb) -> cb.greaterThanOrEqualTo(root.get(first), root.get(second)));
    }


    // between
    public <A extends Comparable<? super A>> Specification<T> between(
            SingularAttribute<T, A> field, A min, A max) {
        return ((root, query, cb) -> cb.between(root.get(field), min, max));
    }


    // apply
    public <T, A> Specification<T> apply(
            Consumer<SingularAttribute<T, A>> consumer, SingularAttribute<T, A> associate) {
        return ((root, query, cb) -> {
            consumer.accept(associate);
            return cb.conjunction();
        });
    }


    // join
    public <T, A> Specification<T> join(SingularAttribute<T, A> associate) {
        return ((root, query, cb) -> {
            apply(root::join, associate);
            return cb.conjunction();
        });
    }
}
