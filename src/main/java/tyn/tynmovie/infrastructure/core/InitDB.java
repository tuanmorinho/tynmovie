package tyn.tynmovie.infrastructure.core;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import tyn.tynmovie.app.common.MovieTagEnum;
import tyn.tynmovie.app.domain.*;
import tyn.tynmovie.app.dto.MovieDBVoteResponse;
import tyn.tynmovie.app.repository.*;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class InitDB {

    @Autowired
    private final ActorRepository actorRepository;
    @Autowired
    private final DirectorRepository directorRepository;
    @Autowired
    private final GenreRepository genreRepository;
    @Autowired
    private final LanguageRepository languageRepository;
    @Autowired
    private final MovieRepository movieRepository;
    @Autowired
    private final NationalRepository nationalRepository;
    @Autowired
    private final RatingRepository ratingRepository;
    @Autowired
    private final CensorRepository censorRepository;

    @Value("${themoviedb.url}")
    private String themoviedbUrl;
    @Value("${themoviedb.api_key}")
    private String themoviedbApiKey;
    @Value("${themoviedb.access_token}")
    private String themoviedbAccessToken;

    @PostConstruct
    private void initMovie() throws ParseException {
        // national
        nationalRepository.deleteAll();
        List<National> nationals = Stream.of("Hoa Kì", "Anh Quốc", "Canada", "Brazil", "Bồ Đào Nha", "Argentina", "Ireland", "Nigeria")
                .map(National::new).toList();
        nationalRepository.saveAll(nationals);

        // rating

        ratingRepository.deleteAll();

        OkHttpClient client = new OkHttpClient();
        ObjectMapper objectMapper = new ObjectMapper();

        List<Request> requests = Stream.<Request>builder()
                // Deadpool
                .add(new Request.Builder()
                        .url(themoviedbUrl + "293660" + "?api_key=" + themoviedbApiKey)
                        .header("Authorization", "Bearer " + themoviedbAccessToken)
                        .build())

                // Nhiệm vụ: Bất khả thi - Nghiệp báo phần 1
                .add(new Request.Builder()
                        .url(themoviedbUrl + "575264" + "?api_key=" + themoviedbApiKey)
                        .header("Authorization", "Bearer " + themoviedbAccessToken)
                        .build())

                // Oppenheimer
                .add(new Request.Builder()
                        .url(themoviedbUrl + "872585" + "?api_key=" + themoviedbApiKey)
                        .header("Authorization", "Bearer " + themoviedbAccessToken)
                        .build())

                // Vệ binh dải Ngân Hà 3
                .add(new Request.Builder()
                        .url(themoviedbUrl + "447365" + "?api_key=" + themoviedbApiKey)
                        .header("Authorization", "Bearer " + themoviedbAccessToken)
                        .build())

                // Người Nhện: Du hành vũ trụ nhện
                .add(new Request.Builder()
                        .url(themoviedbUrl + "569094" + "?api_key=" + themoviedbApiKey)
                        .header("Authorization", "Bearer " + themoviedbAccessToken)
                        .build())

                // Transformers: Quái thú trỗi dậy
                .add(new Request.Builder()
                        .url(themoviedbUrl + "667538" + "?api_key=" + themoviedbApiKey)
                        .header("Authorization", "Bearer " + themoviedbAccessToken)
                        .build())

                // The Flash
                .add(new Request.Builder()
                        .url(themoviedbUrl + "298618" + "?api_key=" + themoviedbApiKey)
                        .header("Authorization", "Bearer " + themoviedbAccessToken)
                        .build())

                .build().toList();

        List<Double> vote_averages = new ArrayList<>();


        for (Request request: requests) {
            try (Response response = client.newCall(request).execute()) {
                if (!response.isSuccessful()) {
                    throw new IOException("Unexpected code: " + response);
                }
                assert response.body() != null;
                Map<String, Object> vote = objectMapper.readValue(response.body().string(), new TypeReference<>() {});
                vote_averages.add((Double) vote.getOrDefault("vote_average", "vote_average"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        List<Rating> ratings = Stream.<Rating>builder()
                .add(new Rating("Đánh giá phim Deadpool được thực hiện bởi IMDb" ,"https://www.imdb.com/title/tt1431045/ratings/?ref_=tt_ov_rt", vote_averages.get(0)))
                .add(new Rating("Đánh giá phim Nhiệm vụ: Bất khả thi - Nghiệp báo phần 1 được thực hiện bởi IMDb" ,"https://www.imdb.com/title/tt15398776/ratings/?ref_=tt_ov_rt", vote_averages.get(1)))
                .add(new Rating("Đánh giá phim Oppenheimer thực hiện bởi IMDb" ,"https://www.imdb.com/title/tt15398776/ratings/?ref_=tt_ov_rt", vote_averages.get(2)))
                .add(new Rating("Đánh giá phim Vệ binh dải Ngân Hà 3 thực hiện bởi IMDb" ,"https://www.imdb.com/title/tt6791350/ratings/?ref_=tt_ov_rt", vote_averages.get(3)))
                .add(new Rating("Đánh giá phim Người Nhện: Du hành vũ trụ nhện thực hiện bởi IMDb" ,"https://www.imdb.com/title/tt9362722/ratings/?ref_=tt_ov_rt", vote_averages.get(4)))
                .add(new Rating("Đánh giá phim Transformers: Quái thú trỗi dậy thực hiện bởi IMDb" ,"https://www.imdb.com/title/tt5090568/ratings/?ref_=tt_ov_rt", vote_averages.get(5)))
                .add(new Rating("Đánh giá phim The Flash thực hiện bởi IMDb" ,"https://www.imdb.com/title/tt0439572/ratings/?ref_=tt_ov_rt", vote_averages.get(6)))
                .build()
                .toList();
        ratingRepository.saveAll(ratings);

        // director & actor
        National america = nationalRepository.findByNational("Hoa Kì").orElseThrow();
        National england = nationalRepository.findByNational("Anh Quốc").orElseThrow();
        National portugal = nationalRepository.findByNational("Bồ Đào Nha").orElseThrow();
        National argentina = nationalRepository.findByNational("Argentina").orElseThrow();
        National canada = nationalRepository.findByNational("Canada").orElseThrow();
        National brazil = nationalRepository.findByNational("Brazil").orElseThrow();
        National ireland = nationalRepository.findByNational("Ireland").orElseThrow();
        National nigeria = nationalRepository.findByNational("Nigeria").orElseThrow();

        directorRepository.deleteAll();
        List<Director> directors = Stream.<Director>builder()
                .add(new Director("Tim", "", "Miller", "10/10/1964", Collections.singleton(america)))
                .add(new Director("Christopher", "", "McQuarrie", "25/10/1968", Collections.singleton(america)))
                .add(new Director("Christopher", "", "Nolan", "30/07/1970", new HashSet<>(Arrays.asList(america, england))))
                .add(new Director("James", "", "Gunn", "05/08/1966", Collections.singleton(america)))
                .add(new Director("Joaquim", "Dos", "Santos", "22/06/1977", Collections.singleton(portugal)))
                .add(new Director("Kemp", "", "Powers", "30/10/1973", Collections.singleton(america)))
                .add(new Director("Justin", "K.", "Thompson", "", new HashSet<>()))
                .add(new Director("Steven", "Caple", "Jr", "16/02/1988", Collections.singleton(america)))
                .add(new Director("Andy", "", "Muschietti", "26/08/1973", Collections.singleton(argentina)))
                .build()
                .toList();
        directorRepository.saveAll(directors);

        actorRepository.deleteAll();
        List<Actor> actors = Stream.<Actor>builder()
                .add(new Actor("Ryan", "Rodney", "Reynolds", "23/10/1976", Collections.singleton(canada)))
                .add(new Actor("Morena", "", "Baccarin", "02/06/1979", Collections.singleton(brazil)))
                .add(new Actor("Todd", "Joseph", "Miller", "04/06/1981", Collections.singleton(america)))
                .add(new Actor("Thomas", "Cruise", "Mapother", "03/07/1962", Collections.singleton(america)))
                .add(new Actor("Hayley", "Elizabeth", "Atwell", "05/04/1982", Collections.singleton(england)))
                .add(new Actor("Irving", "", "Rhames", "12/05/1959", Collections.singleton(america)))
                .add(new Actor("Cillian", "", "Murphy", "25/05/1976",Collections.singleton(ireland)))
                .add(new Actor("Emily", "", "Blunt", "23/02/1983", Collections.singleton(england)))
                .add(new Actor("Matthew", "Paige", "Damon", "08/10/1970", Collections.singleton(america)))
                .add(new Actor("Christopher", "Michael", "Pratt", "21/06/1979", Collections.singleton(america)))
                .add(new Actor("Chukwudi", "", "Iwuji", "15/10/1975", Collections.singleton(nigeria)))
                .add(new Actor("Bradley", "Charles", "Cooper", "05/01/1975", Collections.singleton(america)))
                .add(new Actor("Shameik", "Alti", "Moore", "04/05/1995", Collections.singleton(america)))
                .add(new Actor("Hailee", "", "Steinfeld", "11/12/1996", Collections.singleton(america)))
                .add(new Actor("Brian", "Tyree", "Henry", "31/03/1982", Collections.singleton(america)))
                .add(new Actor("Ezra", "Matthew", "Miller", "30/09/1992", Collections.singleton(america)))
                .add(new Actor("Michael", "John", "Douglas", "05/09/1951", Collections.singleton(america)))
                .add(new Actor("Sasha", "", "Calle", "07/08/1995", Collections.singleton(america)))
                .add(new Actor("Anthony", "Ramos", "Martinez", "01/11/1991", Collections.singleton(america)))
                .add(new Actor("Dominique", "", "Fishback", "22/03/1991", Collections.singleton(america)))
                .add(new Actor("Lauren", "Luna", "Vélez", "02/11/1964", Collections.singleton(america)))
                .build()
                .toList();
        actorRepository.saveAll(actors);


        // language
        languageRepository.deleteAll();
        List<Language> languages = Stream.of("Tiếng Việt", "Tiếng Anh")
                .map(Language::new).toList();
        languageRepository.saveAll(languages);

        // genre
        genreRepository.deleteAll();
        List<Genre> genres = Stream.of("Tình cảm", "Kinh dị", "Hồi hộp", "Hành động", "Phiêu lưu", "Hài hước", "Gia đình", "Hoạt hình", "Tâm lý", "Ly kì", "Trinh thám", "Tội phạm", "Tiểu sử", "Kịch", "Lịch sử", "Hoạt hình", "Viễn tưởng", "Kỳ ảo")
                .map(Genre::new).toList();
        genreRepository.saveAll(genres);

        // censor
        censorRepository.deleteAll();
        List<Censor> censors = Stream.<Censor>builder()
                .add(new Censor("P", "Thích hợp cho mọi độ tuổi", "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/P_Vietnam.svg/120px-P_Vietnam.svg.png"))
                .add(new Censor("K", "Được phổ biến người xem dưới 13 tuổi với điều kiện xem cùng cha mẹ hoặc người giám hộ", ""))
                .add(new Censor("C13", "Cấm người dưới 13 tuổi", "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d2/C13VN.svg/120px-C13VN.svg.png"))
                .add(new Censor("C16", "Cấm người dưới 16 tuổi", "https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/C16_VN.svg/120px-C16_VN.svg.png"))
                .add(new Censor("C18", "Cấm người dưới 18 tuổi", "https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/C18_VN.svg/120px-C18_VN.svg.png"))
                .build().toList();
        censorRepository.saveAll(censors);

        // movie
        Actor mv1_ac1 = actorRepository.findByFirstNameAndMiddleNameAndLastName("Ryan", "Rodney", "Reynolds").orElseThrow();
        Actor mv1_ac2 = actorRepository.findByFirstNameAndMiddleNameAndLastName("Morena", "", "Baccarin").orElseThrow();
        Actor mv1_ac3 = actorRepository.findByFirstNameAndMiddleNameAndLastName("Todd", "Joseph", "Miller").orElseThrow();
        Director mv1_dr1 = directorRepository.findByFirstNameAndMiddleNameAndLastName("Tim", "", "Miller").orElseThrow();
        Language mv1_lg1 = languageRepository.findByLanguage("Tiếng Anh").orElseThrow();
        National mv1_na1 = nationalRepository.findByNational("Hoa Kì").orElseThrow();
        Rating mv1_ra1 = ratingRepository.findByDescription("Đánh giá phim Deadpool được thực hiện bởi IMDb").orElseThrow();
        List<Genre> mv1_ge = genreRepository.findAllByNameIn(Arrays.asList("Hành động", "Hài hước"));
        Censor mv1_cs = censorRepository.findByType("C18").orElseThrow();

        Actor mv2_ac1 = actorRepository.findByFirstNameAndMiddleNameAndLastName("Thomas", "Cruise", "Mapother").orElseThrow();
        Actor mv2_ac2 = actorRepository.findByFirstNameAndMiddleNameAndLastName("Hayley", "Elizabeth", "Atwell").orElseThrow();
        Actor mv2_ac3 = actorRepository.findByFirstNameAndMiddleNameAndLastName("Irving", "", "Rhames").orElseThrow();
        Director mv2_dr1 = directorRepository.findByFirstNameAndMiddleNameAndLastName("Christopher", "", "McQuarrie").orElseThrow();
        Language mv2_lg1 = languageRepository.findByLanguage("Tiếng Anh").orElseThrow();
        National mv2_na1 = nationalRepository.findByNational("Hoa Kì").orElseThrow();
        Rating mv2_ra1 = ratingRepository.findByDescription("Đánh giá phim Nhiệm vụ: Bất khả thi - Nghiệp báo phần 1 được thực hiện bởi IMDb").orElseThrow();
        List<Genre> mv2_ge = genreRepository.findAllByNameIn(Arrays.asList("Hành động", "Phiêu lưu", "Tội phạm"));
        Censor mv2_cs = censorRepository.findByType("C16").orElseThrow();

        Actor mv3_ac1 = actorRepository.findByFirstNameAndMiddleNameAndLastName("Cillian", "", "Murphy").orElseThrow();
        Actor mv3_ac2 = actorRepository.findByFirstNameAndMiddleNameAndLastName("Emily", "", "Blunt").orElseThrow();
        Actor mv3_ac3 = actorRepository.findByFirstNameAndMiddleNameAndLastName("Matthew", "Paige", "Damon").orElseThrow();
        Director mv3_dr1 = directorRepository.findByFirstNameAndMiddleNameAndLastName("Christopher", "", "Nolan").orElseThrow();
        Language mv3_lg1 = languageRepository.findByLanguage("Tiếng Anh").orElseThrow();
        National mv3_na1 = nationalRepository.findByNational("Hoa Kì").orElseThrow();
        National mv3_na2 = nationalRepository.findByNational("Anh Quốc").orElseThrow();
        Rating mv3_ra1 = ratingRepository.findByDescription("Đánh giá phim Oppenheimer thực hiện bởi IMDb").orElseThrow();
        List<Genre> mv3_ge = genreRepository.findAllByNameIn(Arrays.asList("Tiểu sử", "Kịch", "Lịch sử"));
        Censor mv3_cs = censorRepository.findByType("C16").orElseThrow();

        Actor mv4_ac1 = actorRepository.findByFirstNameAndMiddleNameAndLastName("Christopher", "Michael", "Pratt").orElseThrow();
        Actor mv4_ac2 = actorRepository.findByFirstNameAndMiddleNameAndLastName("Chukwudi", "", "Iwuji").orElseThrow();
        Actor mv4_ac3 = actorRepository.findByFirstNameAndMiddleNameAndLastName("Bradley", "Charles", "Cooper").orElseThrow();
        Director mv4_dr1 = directorRepository.findByFirstNameAndMiddleNameAndLastName("James", "", "Gunn").orElseThrow();
        Language mv4_lg1 = languageRepository.findByLanguage("Tiếng Anh").orElseThrow();
        National mv4_na1 = nationalRepository.findByNational("Hoa Kì").orElseThrow();
        Rating mv4_ra1 = ratingRepository.findByDescription("Đánh giá phim Vệ binh dải Ngân Hà 3 thực hiện bởi IMDb").orElseThrow();
        List<Genre> mv4_ge = genreRepository.findAllByNameIn(Arrays.asList("Hành động", "Phiêu lưu", "Hài hước"));
        Censor mv4_cs = censorRepository.findByType("C16").orElseThrow();

        Actor mv5_ac1 = actorRepository.findByFirstNameAndMiddleNameAndLastName("Shameik", "Alti", "Moore").orElseThrow();
        Actor mv5_ac2 = actorRepository.findByFirstNameAndMiddleNameAndLastName("Hailee", "", "Steinfeld").orElseThrow();
        Actor mv5_ac3 = actorRepository.findByFirstNameAndMiddleNameAndLastName("Brian", "Tyree", "Henry").orElseThrow();
        Director mv5_dr1 = directorRepository.findByFirstNameAndMiddleNameAndLastName("Joaquim", "Dos", "Santos").orElseThrow();
        Director mv5_dr2 = directorRepository.findByFirstNameAndMiddleNameAndLastName("Kemp", "", "Powers").orElseThrow();
        Director mv5_dr3 = directorRepository.findByFirstNameAndMiddleNameAndLastName("Justin", "K.", "Thompson").orElseThrow();
        Language mv5_lg1 = languageRepository.findByLanguage("Tiếng Anh").orElseThrow();
        National mv5_na1 = nationalRepository.findByNational("Hoa Kì").orElseThrow();
        Rating mv5_ra1 = ratingRepository.findByDescription("Đánh giá phim Người Nhện: Du hành vũ trụ nhện thực hiện bởi IMDb").orElseThrow();
        List<Genre> mv5_ge = genreRepository.findAllByNameIn(Arrays.asList("Hoạt hình", "Hành động", "Phiêu lưu"));
        Censor mv5_cs = censorRepository.findByType("C13").orElseThrow();

        Actor mv6_ac1 = actorRepository.findByFirstNameAndMiddleNameAndLastName("Anthony", "Ramos", "Martinez").orElseThrow();
        Actor mv6_ac2 = actorRepository.findByFirstNameAndMiddleNameAndLastName("Dominique", "", "Fishback").orElseThrow();
        Actor mv6_ac3 = actorRepository.findByFirstNameAndMiddleNameAndLastName("Lauren", "Luna", "Vélez").orElseThrow();
        Director mv6_dr1 = directorRepository.findByFirstNameAndMiddleNameAndLastName("Steven", "Caple", "Jr").orElseThrow();
        Language mv6_lg1 = languageRepository.findByLanguage("Tiếng Anh").orElseThrow();
        National mv6_na1 = nationalRepository.findByNational("Hoa Kì").orElseThrow();
        Rating mv6_ra1 = ratingRepository.findByDescription("Đánh giá phim Transformers: Quái thú trỗi dậy thực hiện bởi IMDb").orElseThrow();
        List<Genre> mv6_ge = genreRepository.findAllByNameIn(Arrays.asList("Hành động", "Phiêu lưu", "Viễn tưởng"));
        Censor mv6_cs = censorRepository.findByType("C16").orElseThrow();

        Actor mv7_ac1 = actorRepository.findByFirstNameAndMiddleNameAndLastName("Ezra", "Matthew", "Miller").orElseThrow();
        Actor mv7_ac2 = actorRepository.findByFirstNameAndMiddleNameAndLastName("Michael", "John", "Douglas").orElseThrow();
        Actor mv7_ac3 = actorRepository.findByFirstNameAndMiddleNameAndLastName("Sasha", "", "Calle").orElseThrow();
        Director mv7_dr1 = directorRepository.findByFirstNameAndMiddleNameAndLastName("Andy", "", "Muschietti").orElseThrow();
        Language mv7_lg1 = languageRepository.findByLanguage("Tiếng Anh").orElseThrow();
        National mv7_na1 = nationalRepository.findByNational("Hoa Kì").orElseThrow();
        Rating mv7_ra1 = ratingRepository.findByDescription("Đánh giá phim The Flash thực hiện bởi IMDb").orElseThrow();
        List<Genre> mv7_ge = genreRepository.findAllByNameIn(Arrays.asList("Hành động", "Phiêu lưu", "Kỳ ảo"));
        Censor mv7_cs = censorRepository.findByType("C16").orElseThrow();

        movieRepository.deleteAll();
        List<Movie> movies = Stream.<Movie>builder()
                .add(new Movie(
                        "Deadpool",
                        "Deadpool xoay quanh anh chàng Wade Wilson, một người bị ung thư vô phương cứu chữa được thí nghiệm trở thành dị nhân với khả năng phục hồi siêu tốc giống Wolverine, tuy nhiên Deadpool được rất nhiều fan hâm mộ biết đến như là một nhân vật cực kỳ hài hước với cái mồm không bao giờ ngừng nói nhảm của hắn.",
                        "",
                        "",
                        108,
                        "12/02/2016",
                        "08/02/2016",
                        "15/05/2018",
                        "01/06/2018",
                        MovieTagEnum.OUTDATE,
                        new HashSet<>(List.of(mv1_ac1, mv1_ac2, mv1_ac3)),
                        new HashSet<>(List.of(mv1_dr1)),
                        new HashSet<>(List.of(mv1_lg1)),
                        new HashSet<>(List.of(mv1_na1)),
                        mv1_ra1,
                        new HashSet<>(mv1_ge),
                        mv1_cs))
                .add(new Movie(
                        "Nhiệm vụ: Bất khả thi - Nghiệp báo phần 1",
                        "Ethan Hunt cùng các thành viên tổ chức tình báo Impossible Mission Force (IMF) phải truy tìm một vũ khí nguy hiểm trước khi nó rơi vào tay kẻ xấu.",
                        "https://upload.wikimedia.org/wikipedia/vi/thumb/b/b6/Nhi%E1%BB%87m_v%E1%BB%A5_b%E1%BA%A5t_kh%E1%BA%A3_thi_%E2%80%93_Nghi%E1%BB%87p_b%C3%A1o_%E2%80%93_Ph%E1%BA%A7n_1_-_Vietnam_poster.jpg/220px-Nhi%E1%BB%87m_v%E1%BB%A5_b%E1%BA%A5t_kh%E1%BA%A3_thi_%E2%80%93_Nghi%E1%BB%87p_b%C3%A1o_%E2%80%93_Ph%E1%BA%A7n_1_-_Vietnam_poster.jpg",
                        "",
                        163,
                        "08/07/2023",
                        "08/07/2023",
                        "08/07/2023",
                        "01/08/2023",
                        MovieTagEnum.HOT,
                        new HashSet<>(List.of(mv2_ac1, mv2_ac2, mv2_ac3)),
                        new HashSet<>(List.of(mv2_dr1)),
                        new HashSet<>(List.of(mv2_lg1)),
                        new HashSet<>(List.of(mv2_na1)),
                        mv2_ra1,
                        new HashSet<>(mv2_ge),
                        mv2_cs))
                .add(new Movie(
                        "Oppenheimer",
                        "Câu chuyện về nhà khoa học người Mỹ J. Robert Oppenheimer và vai trò của ông trong việc phát triển bom nguyên tử.",
                        "https://upload.wikimedia.org/wikipedia/vi/thumb/2/21/Oppenheimer_%E2%80%93_Vietnam_poster.jpg/220px-Oppenheimer_%E2%80%93_Vietnam_poster.jpg",
                        "",
                        180,
                        "21/07/2023",
                        "11/07/2023",
                        "01/08/2023",
                        "20/08/2023",
                        MovieTagEnum.HOT,
                        new HashSet<>(List.of(mv3_ac1, mv3_ac2, mv3_ac3)),
                        new HashSet<>(List.of(mv3_dr1)),
                        new HashSet<>(List.of(mv3_lg1)),
                        new HashSet<>(List.of(mv3_na1, mv3_na2)),
                        mv3_ra1,
                        new HashSet<>(mv3_ge),
                        mv3_cs))
                .add(new Movie(
                        "Vệ binh dải Ngân Hà 3",
                        "Vẫn quay cuồng vì mất Gamora, Peter Quill tập hợp đội của mình để bảo vệ vũ trụ và bảo vệ vũ trụ của chính họ - một nhiệm vụ có thể đồng nghĩa với sự kết thúc của các Vệ binh nếu không thành công.",
                        "",
                        "",
                        150,
                        "04/05/2023",
                        "03/05/2023",
                        "15/06/2023",
                        "28/06/2023",
                        MovieTagEnum.OUTDATE,
                        new HashSet<>(List.of(mv4_ac1, mv4_ac2, mv4_ac3)),
                        new HashSet<>(List.of(mv4_dr1)),
                        new HashSet<>(List.of(mv4_lg1)),
                        new HashSet<>(List.of(mv4_na1)),
                        mv4_ra1,
                        new HashSet<>(mv4_ge),
                        mv4_cs))
                .add(new Movie(
                        "Người Nhện: Du hành vũ trụ nhện",
                        "Miles Morales phóng xuyên Đa vũ trụ, nơi anh chạm trán với một nhóm Người Nhện chịu trách nhiệm bảo vệ sự tồn tại của nó. Khi các anh hùng xung đột về cách đối phó với mối đe dọa mới, Miles phải xác định lại ý nghĩa của việc trở thành anh hùng.",
                        "",
                        "",
                        140,
                        "01/06/2023",
                        "01/06/2023",
                        "15/06/2023",
                        "28/06/2023",
                        MovieTagEnum.OUTDATE,
                        new HashSet<>(List.of(mv5_ac1, mv5_ac2, mv5_ac3)),
                        new HashSet<>(List.of(mv5_dr1, mv5_dr2, mv5_dr3)),
                        new HashSet<>(List.of(mv5_lg1)),
                        new HashSet<>(List.of(mv5_na1)),
                        mv5_ra1,
                        new HashSet<>(mv5_ge),
                        mv5_cs))
                .add(new Movie(
                        "Transformers: Quái thú trỗi dậy",
                        "Trong những năm 90, một phe mới của Transformers - the Maximals - gia nhập Autobots với tư cách là đồng minh trong trận chiến giành Trái đất.",
                        "",
                        "",
                        127,
                        "09/06/2023",
                        "09/06/2023",
                        "01/07/2023",
                        "25/07/2023",
                        MovieTagEnum.OUTDATE,
                        new HashSet<>(List.of(mv6_ac1, mv6_ac2, mv6_ac3)),
                        new HashSet<>(List.of(mv6_dr1)),
                        new HashSet<>(List.of(mv6_lg1)),
                        new HashSet<>(List.of(mv6_na1)),
                        mv6_ra1,
                        new HashSet<>(mv6_ge),
                        mv6_cs))
                .add(new Movie(
                        "The Flash",
                        "Barry Allen sử dụng siêu tốc độ của mình để thay đổi quá khứ, nhưng nỗ lực cứu gia đình của anh đã tạo ra một thế giới không có siêu anh hùng, buộc anh phải chạy đua giành lấy mạng sống của mình để cứu lấy tương lai.",
                        "https://upload.wikimedia.org/wikipedia/vi/thumb/8/87/The_Flash_2023_VN_poster.jpg/220px-The_Flash_2023_VN_poster.jpg",
                        "",
                        144,
                        "15/06/2023",
                        "16/06/2023",
                        "01/07/2023",
                        "01/08/2023",
                        MovieTagEnum.OUTSTAND,
                        new HashSet<>(List.of(mv7_ac1, mv7_ac2, mv7_ac3)),
                        new HashSet<>(List.of(mv7_dr1)),
                        new HashSet<>(List.of(mv7_lg1)),
                        new HashSet<>(List.of(mv7_na1)),
                        mv7_ra1,
                        new HashSet<>(mv7_ge),
                        mv7_cs))

                .add(new Movie(
                        "The Flash 2",
                        "Barry Allen sử dụng siêu tốc độ của mình để thay đổi quá khứ, nhưng nỗ lực cứu gia đình của anh đã tạo ra một thế giới không có siêu anh hùng, buộc anh phải chạy đua giành lấy mạng sống của mình để cứu lấy tương lai.",
                        "https://upload.wikimedia.org/wikipedia/vi/thumb/8/87/The_Flash_2023_VN_poster.jpg/220px-The_Flash_2023_VN_poster.jpg",
                        "",
                        144,
                        "15/06/2023",
                        "16/06/2023",
                        "01/07/2023",
                        "01/08/2023",
                        MovieTagEnum.OUTSTAND,
                        new HashSet<>(List.of(mv7_ac1, mv7_ac2, mv7_ac3)),
                        new HashSet<>(List.of(mv7_dr1)),
                        new HashSet<>(List.of(mv7_lg1)),
                        new HashSet<>(List.of(mv7_na1)),
                        null,
                        new HashSet<>(mv7_ge),
                        mv7_cs))
                .add(new Movie(
                        "The Flash 3",
                        "Barry Allen sử dụng siêu tốc độ của mình để thay đổi quá khứ, nhưng nỗ lực cứu gia đình của anh đã tạo ra một thế giới không có siêu anh hùng, buộc anh phải chạy đua giành lấy mạng sống của mình để cứu lấy tương lai.",
                        "https://upload.wikimedia.org/wikipedia/vi/thumb/8/87/The_Flash_2023_VN_poster.jpg/220px-The_Flash_2023_VN_poster.jpg",
                        "",
                        144,
                        "15/06/2023",
                        "16/06/2023",
                        "01/07/2023",
                        "01/08/2023",
                        MovieTagEnum.OUTSTAND,
                        new HashSet<>(List.of(mv7_ac1, mv7_ac2, mv7_ac3)),
                        new HashSet<>(List.of(mv7_dr1)),
                        new HashSet<>(List.of(mv7_lg1)),
                        new HashSet<>(List.of(mv7_na1)),
                        null,
                        new HashSet<>(mv7_ge),
                        mv7_cs))
                .add(new Movie(
                        "The Flash 4",
                        "Barry Allen sử dụng siêu tốc độ của mình để thay đổi quá khứ, nhưng nỗ lực cứu gia đình của anh đã tạo ra một thế giới không có siêu anh hùng, buộc anh phải chạy đua giành lấy mạng sống của mình để cứu lấy tương lai.",
                        "https://upload.wikimedia.org/wikipedia/vi/thumb/8/87/The_Flash_2023_VN_poster.jpg/220px-The_Flash_2023_VN_poster.jpg",
                        "",
                        144,
                        "15/06/2023",
                        "16/06/2023",
                        "01/07/2023",
                        "01/08/2023",
                        MovieTagEnum.OUTSTAND,
                        new HashSet<>(List.of(mv7_ac1, mv7_ac2, mv7_ac3)),
                        new HashSet<>(List.of(mv7_dr1)),
                        new HashSet<>(List.of(mv7_lg1)),
                        new HashSet<>(List.of(mv7_na1)),
                        null,
                        new HashSet<>(mv7_ge),
                        mv7_cs))

                .add(new Movie(
                        "The Flash 5",
                        "Barry Allen sử dụng siêu tốc độ của mình để thay đổi quá khứ, nhưng nỗ lực cứu gia đình của anh đã tạo ra một thế giới không có siêu anh hùng, buộc anh phải chạy đua giành lấy mạng sống của mình để cứu lấy tương lai.",
                        "https://upload.wikimedia.org/wikipedia/vi/thumb/8/87/The_Flash_2023_VN_poster.jpg/220px-The_Flash_2023_VN_poster.jpg",
                        "",
                        144,
                        "15/06/2023",
                        "16/06/2023",
                        "01/07/2023",
                        "01/08/2023",
                        MovieTagEnum.OUTSTAND,
                        new HashSet<>(List.of(mv7_ac1, mv7_ac2, mv7_ac3)),
                        new HashSet<>(List.of(mv7_dr1)),
                        new HashSet<>(List.of(mv7_lg1)),
                        new HashSet<>(List.of(mv7_na1)),
                        null,
                        new HashSet<>(mv7_ge),
                        mv7_cs))
                .add(new Movie(
                        "The Flash 6",
                        "Barry Allen sử dụng siêu tốc độ của mình để thay đổi quá khứ, nhưng nỗ lực cứu gia đình của anh đã tạo ra một thế giới không có siêu anh hùng, buộc anh phải chạy đua giành lấy mạng sống của mình để cứu lấy tương lai.",
                        "https://upload.wikimedia.org/wikipedia/vi/thumb/8/87/The_Flash_2023_VN_poster.jpg/220px-The_Flash_2023_VN_poster.jpg",
                        "",
                        144,
                        "15/06/2023",
                        "16/06/2023",
                        "01/07/2023",
                        "01/08/2023",
                        MovieTagEnum.OUTSTAND,
                        new HashSet<>(List.of(mv7_ac1, mv7_ac2, mv7_ac3)),
                        new HashSet<>(List.of(mv7_dr1)),
                        new HashSet<>(List.of(mv7_lg1)),
                        new HashSet<>(List.of(mv7_na1)),
                        null,
                        new HashSet<>(mv7_ge),
                        mv7_cs))
                .add(new Movie(
                        "The Flash 7",
                        "Barry Allen sử dụng siêu tốc độ của mình để thay đổi quá khứ, nhưng nỗ lực cứu gia đình của anh đã tạo ra một thế giới không có siêu anh hùng, buộc anh phải chạy đua giành lấy mạng sống của mình để cứu lấy tương lai.",
                        "https://upload.wikimedia.org/wikipedia/vi/thumb/8/87/The_Flash_2023_VN_poster.jpg/220px-The_Flash_2023_VN_poster.jpg",
                        "",
                        144,
                        "15/06/2023",
                        "16/06/2023",
                        "01/07/2023",
                        "01/08/2023",
                        MovieTagEnum.OUTSTAND,
                        new HashSet<>(List.of(mv7_ac1, mv7_ac2, mv7_ac3)),
                        new HashSet<>(List.of(mv7_dr1)),
                        new HashSet<>(List.of(mv7_lg1)),
                        new HashSet<>(List.of(mv7_na1)),
                        null,
                        new HashSet<>(mv7_ge),
                        mv7_cs))

                .add(new Movie(
                        "The Flash 8",
                        "Barry Allen sử dụng siêu tốc độ của mình để thay đổi quá khứ, nhưng nỗ lực cứu gia đình của anh đã tạo ra một thế giới không có siêu anh hùng, buộc anh phải chạy đua giành lấy mạng sống của mình để cứu lấy tương lai.",
                        "https://upload.wikimedia.org/wikipedia/vi/thumb/8/87/The_Flash_2023_VN_poster.jpg/220px-The_Flash_2023_VN_poster.jpg",
                        "",
                        144,
                        "15/06/2023",
                        "16/06/2023",
                        "01/07/2023",
                        "01/08/2023",
                        MovieTagEnum.OUTSTAND,
                        new HashSet<>(List.of(mv7_ac1, mv7_ac2, mv7_ac3)),
                        new HashSet<>(List.of(mv7_dr1)),
                        new HashSet<>(List.of(mv7_lg1)),
                        new HashSet<>(List.of(mv7_na1)),
                        null,
                        new HashSet<>(mv7_ge),
                        mv7_cs))
                .add(new Movie(
                        "The Flash 9",
                        "Barry Allen sử dụng siêu tốc độ của mình để thay đổi quá khứ, nhưng nỗ lực cứu gia đình của anh đã tạo ra một thế giới không có siêu anh hùng, buộc anh phải chạy đua giành lấy mạng sống của mình để cứu lấy tương lai.",
                        "https://upload.wikimedia.org/wikipedia/vi/thumb/8/87/The_Flash_2023_VN_poster.jpg/220px-The_Flash_2023_VN_poster.jpg",
                        "",
                        144,
                        "15/06/2023",
                        "16/06/2023",
                        "01/07/2023",
                        "01/08/2023",
                        MovieTagEnum.OUTSTAND,
                        new HashSet<>(List.of(mv7_ac1, mv7_ac2, mv7_ac3)),
                        new HashSet<>(List.of(mv7_dr1)),
                        new HashSet<>(List.of(mv7_lg1)),
                        new HashSet<>(List.of(mv7_na1)),
                        null,
                        new HashSet<>(mv7_ge),
                        mv7_cs))
                .add(new Movie(
                        "The Flash 10",
                        "Barry Allen sử dụng siêu tốc độ của mình để thay đổi quá khứ, nhưng nỗ lực cứu gia đình của anh đã tạo ra một thế giới không có siêu anh hùng, buộc anh phải chạy đua giành lấy mạng sống của mình để cứu lấy tương lai.",
                        "https://upload.wikimedia.org/wikipedia/vi/thumb/8/87/The_Flash_2023_VN_poster.jpg/220px-The_Flash_2023_VN_poster.jpg",
                        "",
                        144,
                        "15/06/2023",
                        "16/06/2023",
                        "01/07/2023",
                        "01/08/2023",
                        MovieTagEnum.OUTSTAND,
                        new HashSet<>(List.of(mv7_ac1, mv7_ac2, mv7_ac3)),
                        new HashSet<>(List.of(mv7_dr1)),
                        new HashSet<>(List.of(mv7_lg1)),
                        new HashSet<>(List.of(mv7_na1)),
                        null,
                        new HashSet<>(mv7_ge),
                        mv7_cs))

                .build()
                .toList();
        movieRepository.saveAll(movies);
    }
}