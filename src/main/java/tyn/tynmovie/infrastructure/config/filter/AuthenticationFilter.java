package tyn.tynmovie.infrastructure.config.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import tyn.tynmovie.infrastructure.config.exception.RestException;
import tyn.tynmovie.infrastructure.core.User;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Map.entry;

@Component
@RequiredArgsConstructor
public class AuthenticationFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(
            @NonNull HttpServletRequest request,
            @NonNull HttpServletResponse response,
            @NonNull FilterChain filterChain
    ) throws ServletException, IOException {
        try {
            final String username = getUsernameFromRequest(request);
            final List<String> roles = getRolesFromRequest(request);

            Set<SimpleGrantedAuthority> authorities = new HashSet<>();
            if (roles != null && !roles.isEmpty()) {
                authorities = roles.stream().filter(StringUtils::hasText).map(SimpleGrantedAuthority::new).collect(Collectors.toSet());
            }

            if (verifyUser(username) && !authorities.isEmpty() && SecurityContextHolder.getContext().getAuthentication() == null) {
                UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                        username,
                        null,
                        authorities
                );
                authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authToken);
            }
            filterChain.doFilter(request, response);
        } catch (RuntimeException e) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
        }
    }

    private String getUsernameFromRequest(HttpServletRequest request) {
        final String username = request.getHeader("username");
        if (StringUtils.hasText(username) && username.startsWith("Bearer ")) {
            return username.substring(7);
        }
        return null;
    }

    private List<String> getRolesFromRequest(HttpServletRequest request) {
        final String rolesString = request.getHeader("roles");
        if (StringUtils.hasText(rolesString) && rolesString.startsWith("Bearer ")) {
            return Arrays.asList(rolesString.substring(8, rolesString.length() - 1).split(", "));
        }
        return null;
    }

    private Boolean verifyUser(String username) {
        if (!StringUtils.hasText(username)) return false;

        Request request = new Request.Builder()
                .url("http://localhost:8080/user/internal/getUser/" + username)
                .build();
        OkHttpClient client = new OkHttpClient();

        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new RestException("error.unauthorized", HttpStatus.UNAUTHORIZED);
            }
            if (response.body() != null) {
                ObjectMapper mapper = new ObjectMapper();
                final User user = mapper.readValue(response.body().string(), User.class);
                return !Objects.isNull(user);
            }
            return false;
        } catch (Exception e) {
            throw new RestException("error.unauthorized", HttpStatus.UNAUTHORIZED);
        }
    }
}
